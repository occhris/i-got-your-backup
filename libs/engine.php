<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

class Engine
{
    protected $self_dir;

    public function __construct()
    {
        $this->self_dir = dirname(__DIR__);
    }

    public function calculate_next_backup($last_time, $backup_every_days, $start_time)
    {
        if ($backup_every_days === null) {
            return null;
        }

        if ($last_time === null) {
            $next_time = time();
        } else {
            $next_time = $last_time + $backup_every_days * 60 * 60 * 24;
        }

        // Adjust timestamp to next at given hour

        $month = intval(date('m', $next_time));
        $day = intval(date('d', $next_time));
        $year = intval(date('Y', $next_time));

        list($hours, $minutes) = array_map('intval', explode(':', $start_time));

        $timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

        if ($timestamp <= time()) {
            $timestamp = mktime($hours, $minutes, 0, $month, $day + 1, $year);
        }

        return $timestamp;
    }

    public function cleanup_old_backups($delete_older_than_days)
    {
        if ($delete_older_than_days === null) {
            return;
        }

        $cutoff = time() - $delete_older_than_days * 60 * 60 * 24;
        $files = scandir('backups');
        foreach ($files as $filename) {
            if (in_array($filename, ['.', '..', '.htaccess'])) {
                continue;
            }

            $path = 'backups/' . $filename;
            if (filemtime($path) < $cutoff) {
                unlink($path);
            }
        }
    }

    public function do_file_backup($filename_pattern, $include_paths, $include_globs, $exclude_paths, $exclude_regexps, $exclude_globs, $compression, &$log_file)
    {
        $exclude_paths = array_flip($exclude_paths);

        $lock_message = get_lock_message();
        if ($lock_message !== null) {
            throw new Exception($lock_message);
        }

        $filename = strftime($filename_pattern);
        switch ($compression) {
            case 'none':
                $filename .= '.tar.tmp';
                break;
            case 'gzip':
                $filename .= '.tar.gz.tmp';
                break;
            case 'bzip':
                $filename .= '.tar.bz2.tmp';
                break;
            case 'xz':
                $filename .= '.tar.xz.tmp';
                break;
        }
        $archive_path = 'backups/' . $filename;

        file_put_contents('data/lock.bin', 'Backing up to ' . $filename);
        $list_file = fopen('data/files_list.bin', 'wb');

        $this->write_log_line($log_file, 'Backing up to ' . $archive_path);

        $this->write_log_line($log_file, 'Start by building list of files to include, honouring excludes (data/files_list.bin)');

        foreach ($include_paths as $include_path) {
            $this->do_file_backup__path($list_file, $include_path, $exclude_paths, $exclude_regexps, $exclude_globs, $log_file, true);
        }

        foreach ($include_globs as $include_glob) {
            $paths = glob($include_glob);
            foreach ($paths as $path) {
                $this->do_file_backup__path($list_file, $path, $exclude_paths, $exclude_regexps, $exclude_globs, $log_file);
            }
        }

        fclose($list_file);

        $tar_options = 'vP';
        switch ($compression) {
            case 'gzip':
                $tar_options .= 'z';
                break;
            case 'bzip':
                $tar_options .= 'j';
                break;
            case 'xz':
                $tar_options .= 'J';
                break;
        }
        $cmd = 'tar -cT data/files_list.bin -' . $tar_options . 'f ' . escapeshellarg($archive_path);
        $this->write_log_line($log_file, $cmd, true);

        unlink('data/files_list.bin');

        file_put_contents('data/lock.bin', 'Compressing ' . $filename);

        if (is_file($archive_path)) {
            $archive_path_new = substr($archive_path, 0, strlen($archive_path) - 4);
            rename($archive_path, $archive_path_new); // Strip .tmp
        }

        unlink('data/lock.bin');
    }

    protected function do_file_backup__path($list_file, $path, $exclude_paths, $exclude_regexps, $exclude_globs, &$log_file, $top_level = false)
    {
        if ((!$top_level) && ($this->is_path_excluded($path, $exclude_paths, $exclude_regexps, $exclude_globs))) {
            $this->write_log_line($log_file, 'Excluded ' . $path);
            return;
        }

        static $done_already = [];
        if (isset($done_already[$path])) {
            return;
        }
        $done_already[$path] = true;

        if (!file_exists($path)) {
            $this->write_log_line($log_file, 'Could not find path, ' . $path);
            return;
        } elseif (!is_dir($path)) { // Probably a file, but could be a symlink
            fwrite($list_file, $path . "\n");
        } else { // Directory
            $_files = @scandir($path);
            if ($_files === false) {
                $this->write_log_line($log_file, 'Could not read path, ' . $path);

                return;
            }
            $is_empty = true;
            foreach ($_files as $file) {
                if (in_array($file, ['.', '..'])) {
                    continue;
                }

                $is_empty = false;

                $_path = $path . '/' . $file;

                if ($this->is_path_excluded($_path, $exclude_paths, $exclude_regexps, $exclude_globs)) {
                    $this->write_log_line($log_file, 'Excluded ' . $_path);
                    continue;
                }

                if (!is_dir($_path)) {
                    if (isset($done_already[$_path])) {
                        continue;
                    }
                    $done_already[$_path] = true;

                    fwrite($list_file, $_path . "\n");
                } else {
                    $this->do_file_backup__path($list_file, $_path, $exclude_paths, $exclude_regexps, $exclude_globs, $log_file);
                }
            }

            if ($is_empty) {
                // Add explicit empty directory, as we have not made it implicit by putting something into it
                //  Note we do not support putting in empty directories that are only empty due to skips, because we have no reasonably easy way of communicating that to TAR
                fwrite($list_file, $path . "\n");
            }
        }
    }

    protected function is_path_excluded($path, $exclude_paths, $exclude_regexps, $exclude_globs)
    {
        if ($path == $this->self_dir) {
            return true;
        }

        $is_file = is_file($path);

        if (isset($exclude_paths[$path])) {
            return true;
        }

        foreach ($exclude_regexps as $_regexp) {
            if (substr($_regexp, 0, 1) != '#') {
                $__regexp = '#' . $_regexp . '#';
            } else {
                $__regexp = $_regexp;
            }
            if (preg_match($__regexp, $path) != 0) {
                return true;
            }
        }

        foreach ($exclude_globs as $_glob) {
            if (in_array($path, glob($_glob))) {
                return true;
            }
            if (strpos($_glob, '/') === false) {
                if (in_array($path, glob(dirname($path) . '/' . $_glob))) {
                    return true;
                }
            }
        }

        return false;
    }

    public function do_database_backup($filename_pattern, $db_name, $db_username, $db_password, $db_host, $compression, &$log_file)
    {
        $lock_message = get_lock_message();
        if ($lock_message !== null) {
            throw new Exception($lock_message);
        }

        $filename = strftime($filename_pattern) . '--' . $db_name . '.sql.tmp';
        $sql_path = 'backups/' . $filename;

        file_put_contents('data/lock.bin', 'Backing up DB to ' . $filename);

        $this->write_log_line($log_file, 'Backing up DB to ' . $sql_path);

        $cmd = 'mysqldump -u' . escapeshellarg($db_username) . ' -h' . escapeshellarg($db_host);
        if ($db_password != '') {
            $cmd .= ' -p' . escapeshellarg($db_password);
        }
        $cmd .= ' ' . escapeshellarg($db_name);
        $this->write_log_line($log_file, $cmd . ' 2>&1 > ' . escapeshellarg($sql_path), true);

        file_put_contents('data/lock.bin', 'Compressing ' . $filename);

        if (is_file($sql_path)) {
            $sql_path_new = substr($sql_path, 0, strlen($sql_path) - 4);
            rename($sql_path, $sql_path_new); // Strip .tmp

            if ($compression != 'non') {
                switch ($compression) {
                    case 'gzip':
                        $cmd = 'gzip ' . escapeshellarg($sql_path_new);
                        break;
                    case 'bzip':
                        $cmd = 'bzip2 ' . escapeshellarg($sql_path_new);
                        break;
                    case 'xz':
                        $cmd = 'xz ' . escapeshellarg($sql_path_new);
                        break;
                }
                shell_exec($cmd);
                $this->write_log_line($log_file, 'Compressed SQL file, ' . $sql_path_new);
            }
        }

        unlink('data/lock.bin');
    }

    public function do_command($completion_command, &$log_file)
    {
        $this->write_log_line($log_file, $completion_command, true);
    }

    protected function write_log_line($log_file, $line, $shell_exec = false)
    {
        if ($shell_exec) {
            fwrite($log_file, display_time(time()) . ' Executing ' . $line . "\n");
            echo 'Executing ' . $line . "\n";

            $results = shell_exec($line . ' 2>&1');
            $line = 'Command result (if any):' . "\n" . trim($results);

            fwrite($log_file, display_time(time()) . ' ' . $line . "\n");
            echo $line . "\n";
        } else {
            $line = trim($line);
            if ($line == '') {
                return;
            }

            fwrite($log_file, display_time(time()) . ' ' . $line . "\n");
            echo $line . "\n";
        }
    }
}
