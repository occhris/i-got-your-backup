<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

class Options
{
    protected $options = [
        'password' => ['New password', '', 'password_input'],

        'delete_older_than_days' => ['Delete backups/logs older than this many days', null, 'number_input'],

        'filename_pattern' => ['Filename pattern (strftime format)', 'backup-%Y-%m-%d--%H-%M', 'line_input'],

        'archive_compression' => ['Compression to use on the files archive', 'gzip', 'list_input', ['none', 'gzip', 'bzip', 'xz']],
        'database_compression' => ['Compression to use on the database', 'gzip', 'list_input', ['none', 'gzip', 'bzip', 'xz']],

        'backup_every_days' => ['Backup every days', null, 'number_input'],
        'start_time' => ['Recurring backup start time', '00:00', 'time_input'],

        'include_paths' => ['Include paths', [], 'multiline_input'],
        'include_globs' => ['Include globs (wildcard expressions)', [], 'multiline_input'],

        'exclude_paths' => ['Exclude paths', [], 'multiline_input'],
        'exclude_regexps' => ['Exclude regexps (unanchored by default)', [], 'multiline_input'],
        'exclude_globs' => ['Exclude globs (wildcard expressions)', [], 'multiline_input'],

        'databases' => ['Databases', [], 'text_tabular_input', ['Database name', 'Username', 'Password', 'Hostname'], ['db_name', 'db_username', 'db_password', 'db_host']],

        'completion_command' => ['Completion shell command', '', 'line_input'],
    ];

    public function read_options_from_config()
    {
        $settings = @json_decode(file_get_contents('data/_config.bin'), true);
        if (!is_array($settings)) {
            $settings = [];
        }
        foreach ($this->options as $key => $details) {
            if (!isset($settings[$key])) {
                $settings[$key] = $details[1];
            }
        }
        return $settings;
    }

    public function options_ui()
    {
        $settings = $this->read_options_from_config();

        foreach ($this->options as $key => $details) {
            call_user_func_array('do_' . $details[2], array_merge([$details[0], $key, $settings[$key]], array_slice($details, 3)));
        }
    }

    public function read_options_from_post()
    {
        $settings = [];
        foreach ($this->options as $key => $details) {
            $settings[$key] = call_user_func_array('read_' . $details[2], array_merge([$key], array_slice($details, 3)));
        }
        return $settings;
    }

    public function save_options_from_post()
    {
        file_put_contents('data/_config.bin', json_encode($this->read_options_from_post()));
    }
}
