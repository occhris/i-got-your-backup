<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

function password_check()
{
    if (isset($_POST['login_password'])) {
        $options_ob = new Options();
        $settings = $options_ob->read_options_from_config();
        $password = $settings['password'];

        if ($password != $_POST['login_password']) {
            login_form('Incorrect password.');
        }
    } else {
        login_form();
    }
}

function login_form($message = 'Access restricted.')
{
    do_header('Log in');

    echo <<<END
<form action="#" method="POST">
    <p>{$message}</p>
END;
    do_password_input('Password', 'login_password');
    echo <<<END

    <input type="submit" value="Log in" />
</form>
END;

    do_footer();

    exit();
}

function get_next_time($force_update = false)
{
    $next = @file_get_contents('data/next.bin');
    if ($next === false) {
        $next = calculate_next_time();

        return $next;
    }

    return intval($next);
}

function calculate_next_time($last_time = null)
{
    $options_ob = new Options();
    $settings = $options_ob->read_options_from_config();
    $backup_every_days = $settings['backup_every_days'];
    $start_time = $settings['start_time'];

    $engine_ob = new Engine();
    $next = $engine_ob->calculate_next_backup($last_time, $backup_every_days, $start_time);

    if ($next !== null) {
        file_put_contents('data/next.bin', strval($next));
    }

    return $next;
}

function display_time($timestamp)
{
    return strftime('%Y-%m-%d %H:%M', $timestamp);
}

function get_lock_message()
{
    if (is_file('data/lock.bin')) {
        return 'Backup currently in process, ' . file_get_contents('data/lock.bin') . ' @ ' . strftime('%Y-%m-%d %H:%M', filemtime('data/lock.bin'));
    }
    return null;
}
