<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

function do_header($heading = 'I got your backup')
{
    echo <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>I got your backup</title>
    <link href="styles.css" rel="stylesheet" />

    <meta name="robots" content="noindex" />
</head>
<body>
    <h1>{$heading}</h1>
END;
}

function do_password_input($label, $name, $value = '')
{
    $_value = htmlentities($value);
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <input type="password" name="{$name}" id="{$name}" value="{$_value}" />
    </p>
END;
}

function read_password_input($name)
{
    return $_POST[$name];
}

function do_line_input($label, $name, $value = '')
{
    $_value = htmlentities($value);
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <input type="text" name="{$name}" id="{$name}" value="{$_value}" />
    </p>
END;
}

function read_line_input($name)
{
    return $_POST[$name];
}

function do_number_input($label, $name, $value = '')
{
    $_value = htmlentities($value);
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <input type="number" name="{$name}" id="{$name}" value="{$_value}" />
    </p>
END;
}

function read_number_input($name)
{
    return empty($_POST[$name]) ? null : $_POST[$name];
}

function do_time_input($label, $name, $value = '')
{
    $_value = htmlentities($value);
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <input type="time" name="{$name}" id="{$name}" value="{$_value}" />
    </p>
END;
}

function read_time_input($name)
{
    return empty($_POST[$name]) ? null : $_POST[$name];
}

function do_list_input($label, $name, $value, $list)
{
    $_value = htmlentities($value);
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <select name="{$name}" id="{$name}">
END;
    foreach ($list as $item) {
        $_item = htmlentities($item);
        $_selected = ($item == $value) ? ' selected="selected"' : '';
        echo <<<END
            <option{$_selected}>{$_item}</option>
END;
    }
    echo <<<END
        </select>
    </p>
END;
}

function read_list_input($name)
{
    return empty($_POST[$name]) ? null : $_POST[$name];
}

function do_multiline_input($label, $name, $value = [])
{
    $_value = htmlentities(implode("\n", $value));
    echo <<<END
    <p>
        <label for="{$name}">{$label}</label>
        <textarea name="{$name}" id="{$name}">{$_value}</textarea>
    </p>
END;
}

function read_multiline_input($name)
{
    $value = [];
    $lines = array_map('trim', explode("\n", $_POST[$name]));
    foreach ($lines as $line) {
        if (trim($line) != '') {
            $value[] = $line;
        }
    }
    return $value;
}

function do_text_tabular_input($set_label, $set_name, $values, $labels, $names)
{
    $count = count($labels);
    echo <<<END
    <table>
        <thead>
            <tr>
                <th colspan="{$count}">{$set_label}</th>
            </tr>
            <tr>
END;
    foreach ($labels as $i => $label) {
        $name = $names[$i];
        echo <<<END
                <th>
                    <label for="{$set_name}__{$name}">{$label}</label>
                </th>
END;
    }
    echo <<<END
            </tr>
        </thead>
        <tbody>
            <tr>
END;
    foreach ($labels as $i => $label) {
        $name = $names[$i];
        $value = '';
        foreach ($values as $__value) {
            if ($value != '') {
                $value .= "\n";
            }
            $value .= $__value[$name];
        }
        $_value = htmlentities($value);
        echo <<<END
                <td>
                    <textarea name="{$set_name}__{$name}" id="{$set_name}__{$name}">{$_value}</textarea>
                </td>
END;
    }
    echo <<<END
            </tr>
        </tbody>
    </table>
END;
}

function read_text_tabular_input($set_name, $labels, $names)
{
    $values = [];
    $columns = [];
    $min_values = null;
    $max_values = null;
    foreach ($names as $name) {
        $columns[$name] = array_map('trim', explode("\n", $_POST[$set_name . '__' . $name]));
        if (($min_values === null) || (count($columns[$name]) < $min_values)) {
            $min_values = count($columns[$name]);
        }
        if (($max_values === null) || (count($columns[$name]) > $max_values)) {
            $max_values = count($columns[$name]);
        }
    }
    if ($min_values == 0) {
        return [];
    }
    for ($i = 0; $i < $max_values; $i++) {
        $value = [];
        foreach (array_keys($columns) as $name) {
            $value[$name] = isset($columns[$name][$i]) ? $columns[$name][$i] : $columns[$name][0];
        }
        $values[] = $value;
    }
    return $values;
}

function do_back_button($_password)
{
    echo <<<END
<hr />

<form action="index.php" method="POST">
    <input type="hidden" name="login_password" value="{$_password}" />
    <input type="submit" value="&laquo; Back" />
</form>
END;
}

function do_footer()
{
    echo <<<END
</body>
</html>
END;
}
