<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

$filename = $_POST['filename'];

set_time_limit(0);

$path = 'backups/' . $filename;

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . urlencode($filename) . '"');
header('Content-Length: ' . filesize($path));

readfile($path);
