<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

if (php_sapi_name() != 'cli') {
    throw new Exception('May only be called on CLI');
}

$options_ob = new Options();
$settings = $options_ob->read_options_from_config();
$backup_every_days = $settings['backup_every_days'];
$start_time = $settings['start_time'];
$delete_older_than_days = $settings['delete_older_than_days'];

$next_time = get_next_time();

$options = getopt('f');

if (($next_time > time()) && (!isset($options['f']))) {
    return; // Nothing to do
}

$engine_ob = new Engine();

$engine_ob->cleanup_old_backups($delete_older_than_days);

$filename_pattern = $settings['filename_pattern'];
$include_paths = $settings['include_paths'];
$include_globs = $settings['include_globs'];
$exclude_paths = $settings['exclude_paths'];
$exclude_regexps = $settings['exclude_regexps'];
$exclude_globs = $settings['exclude_globs'];
$completion_command = $settings['completion_command'];
$archive_compression = $settings['archive_compression'];
$database_compression = $settings['database_compression'];

$log_file = fopen('backups/' . strftime($filename_pattern) . '.log', 'wb');

$engine_ob->do_file_backup($filename_pattern, $include_paths, $include_globs, $exclude_paths, $exclude_regexps, $exclude_globs, $archive_compression, $log_file);
foreach ($settings['databases'] as $database) {
    $engine_ob->do_database_backup($filename_pattern, $database['db_name'], $database['db_username'], $database['db_password'], $database['db_host'], $database_compression, $log_file);
}
$engine_ob->do_command($completion_command, $log_file);

fclose($log_file);

calculate_next_time(time());
