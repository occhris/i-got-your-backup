<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

$filename = $_POST['filename'];

unlink('backups/' . $filename);

if (isset($_GET['offset'])) {
    // Called by AJAX...

    header('Content-type: text/plain');

    $offset = intval($_GET['offset']);

    $data = file_get_contents('backups/' . $filename, false, null, $offset);

    echo $data;

    return;
}

// UI

$_password = htmlentities($_POST['login_password']);
$_filename = htmlentities($filename);

do_header('Deleted ' . htmlentities($filename));

echo <<<END
<p class="message">Deleted {$_filename}.</p>
END;

do_back_button($_password);

do_footer();
