<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

do_header();

$lock_message = get_lock_message();
$next_time = get_next_time();

if ($lock_message === null) {
    if ($next_time === null) {
        $status = 'No backup scheduled';
    } else {
        $status = 'Scheduled for: ' . display_time($next_time) . ' UTC';
    }
} else {
    $status = $lock_message;
}

$_password = htmlentities($_POST['login_password']);
$_password_urlencoded = urlencode($_POST['login_password']);

echo <<<END
    <h2>Current status</h2>

    <p>{$status}</p>

    <h2>Existing backups</h2>

    <script>
        var de = document.documentElement;

        function refresh_page()
        {
            fetch('index.php', {
                method: 'POST',
                headers: {
                    "Content-type": "application/x-www-form-urlencoded"
                },
                body: 'login_password={$_password_urlencoded}'
            })
            .then(res => {
                res.text()
                .then(text => {
                    de.innerHTML = text.replace(/<html.*?>/, '').replace(/<\/html>/, '');
                });
            })
            .catch(err => {
                console.log('Error message: ', err);
            });
        }

        window.setInterval(function() {
            refresh_page();
        }, 3000);

        refresh_page();
    </script>

    <table>
        <thead>
            <tr>
                <th>Filename</th>
                <th>Modified</th>
                <th>Size</th>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
END;

$files = scandir('backups');
$has_backup = false;
foreach ($files as $filename) {
    if (in_array($filename, ['.', '..', '.htaccess'])) {
        continue;
    }

    $_filename = htmlentities($filename);
    $_modified = display_time(filemtime('backups/' . $filename));
    $size = filesize('backups/' . $filename);
    $sizes = ['GB' => 1024 * 1024 * 1024, 'MB' => 1024 * 1024, 'KB' => 1024];
    $_size = null;
    foreach ($sizes as $suffix => $div) {
        if ($size > $div) {
            $_size = number_format(round($size / $div, 2), 2) . $suffix;
            break;
        }
    }
    if ($_size === null) {
        $_size = number_format($size) . 'B';
    }
    $script = (substr($filename, -4) == '.log') ? 'logwatch.php' : 'download.php';
    $script_label = (substr($filename, -4) == '.log') ? 'View' : 'Download';
    echo <<<END
            <tr>
                <td>{$_filename}</td>
                <td>{$_modified} UTC</td>
                <td>{$_size}</td>
                <td>
                    <form action="{$script}" method="post">
                        <input type="hidden" name="login_password" value="{$_password}" />
                        <input type="hidden" name="filename" value="{$_filename}" />
                        <input type="submit" value="{$script_label}" onmousedown="this.form.action='{$script}';" />
                        <input type="submit" value="Delete" class="dangerous-button" onmousedown="this.form.action='delete.php';" onclick="return window.confirm('Are you sure you wish to delete {$_filename}?');" />
                    </form>
                </td>
            </tr>
END;

    $has_backup = true;
}

if (!$has_backup) {
    echo <<<END
        <tr>
            <td colspan="4">
                No backups.
            </td>
        </tr>
END;
}

echo <<<END
        </tbody>
    </table>
END;

echo <<<END
    <h2>Tools</h2>

    <form action="config.php" method="post">
        <input type="hidden" name="login_password" value="{$_password}" />
        <p><input type="submit" value="Configuration" /></p>
    </form>

    <form action="trigger.php" method="post">
        <input type="hidden" name="login_password" value="{$_password}" />
        <p><input type="submit" value="Force backup on next Cron cycle" /></p>
    </form>
END;

do_footer();
