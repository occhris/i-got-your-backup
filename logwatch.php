<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

$filename = $_POST['filename'];

if (isset($_GET['offset'])) {
    // Called by AJAX...

    header('Content-type: text/plain');

    $offset = intval($_GET['offset']);

    $data = file_get_contents('backups/' . $filename, false, null, $offset);

    echo $data;

    return;
}

// UI

$_password = htmlentities($_POST['login_password']);
$_password_urlencoded = urlencode($_POST['login_password']);
$_filename_urlencoded = urlencode($filename);

do_header(htmlentities($filename));

echo <<<END
<textarea readonly="readonly" rows="30" id="log" name="log"></textarea>

<script>
    var log = document.getElementById('log');
    var offset = 0;

    function update_log()
    {
        fetch('logwatch.php?offset=' + offset, {
            method: 'POST',
            headers: {
                "Content-type": "application/x-www-form-urlencoded"
            },
            body: 'login_password={$_password_urlencoded}&filename={$_filename_urlencoded}'
        })
        .then(res => {
            res.text()
            .then(text => {
                offset += text.length;
                log.value += text;
            });
        })
        .catch(err => {
            console.log('Error message: ', err);
        });
    }

    window.setInterval(function() {
        update_log();
    }, 3000);

    update_log();
</script>
END;

do_back_button($_password);

do_footer();
