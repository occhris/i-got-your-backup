<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

do_header('Configuration');

$options_ob = new Options();

if (@$_GET['save'] == '1') {
    if ($_POST['password'] == '') {
        $_POST['password'] = $_POST['login_password'];
    }

    $options_ob->save_options_from_post();

    echo '<p class="message">Saved.</p>';

    $_password = htmlentities($_POST['password']);
} else {
    $_password = htmlentities($_POST['login_password']);
}

echo <<<END
<form action="?save=1" method="POST">
    <input type="hidden" name="login_password" value="{$_password}" />
END;

$options_ob->options_ui();

echo <<<END
    <input type="submit" value="Save" />
</form>
END;

do_back_button($_password);

do_footer();
