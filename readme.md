I got your backup
=================

This is a flexible backup system for doing full backups on a web server as configured.

It is not designed to work on ANY server. It's developed for LAMP only, and servers need to support suEXEC, and have basic tools like tar and mysqldump in the path.

Some features:
* Web UI to configure
* AJAX auto-refreshes list of backups
* Schedule backups or run immediately
* Full progress logging, with log watching
* Automatically excludes backups from backups
* Secure: Stops direct web URL access to backups, Stops bots from indexing the UI

To configure:
* Set up Cron to call "php /path/to/cron.php" every minute (so that it can start soon after scheduled to)
* Call the URL to index.php and set it up (the default password is blank, and you should definitely change it)
* The 'data' directory needs to be writable to the web user

Future improvements
-------------------

Contributors could consider adding:
* Incremental support
* Support for configuring multiple sets of configuration
* Windows support
* Support for databases other than MySQL
* Support adding empty directories that are only empty due to skipping all the contents (currently these are omitted)
* Allow specifying compression level https://superuser.com/questions/305128/how-to-specify-level-of-compression-when-using-tar-zcvf
