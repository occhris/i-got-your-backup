<?php /*
 I got your backup

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);

require_once('libs/init.php');

password_check();

do_header('Backup triggered');

file_put_contents('data/next.bin', strval(time()));

echo '<p class="message">Backup will be triggered the next time Cron runs (unless a backup is already running).</p>';

$_password = htmlentities($_POST['login_password']);
do_back_button($_password);

do_footer();
